﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemySystem : MonoBehaviour // скрипт для логики врага
{
    private bool flag; // переменная для активации передвижения врага к игроку, в случае попадания в область видимости противника
    public Text textHp; // переменная, содержащая текст с кол-вом хитпоинтов нашего игрока
    public Animator anim; // переменная, содержащая Animator для обращения к его функциям
    static public int enemyHp = 2; // статическая переменная с кол-вом хп у врага
    private float posToPlayer; // как в случае с игроком, мы имеем такую же переменную с расстоянием от противника до игрока, но уже для противника, чтобы он тоже смог нас атаковать
    public GameObject player; // объект с именем Player
    private Vector2 direction = new Vector2(-0.02f, 0); // скорость с которой наш енеми перемещается по карте

    void OnTriggerEnter2D(Collider2D Obj)
    {
        if (Obj.tag == "FlipTrigger")// проверка на столкновение противника с триггером поворота, они нужны нам для того, чтобы наш противник патрулировал между точками
        {
            Flip();// делаем флип
        }

        if (Obj.tag == "Player") // если триггер противника найдет игрока то мы запускаем функцию передвижения к игроку
        {
            flag = true; // устанавлием флаг
        }
    }

    void OnDestroy() // чтобы обнулить хп для всех последующих объектов при смерти текущего мы устанавливаем переменную в 2 после разрушения текущего объекта
    {
        enemyHp = 2;
    }

    void Update()
    {
        textHp.text = "HP: " + MainSystem.playerHp; // заносим в нашу переменную, содержащую текст хп кол-во нашего хп у игрока

        if (MainSystem.playerHp <= 0) // если хп стало равно нулю, игрок пропадает и появляется надпись GAME OVER
        {
            textHp.text = "GAME OVER";
        }

        if (flag)// если игрок вошел в триггер то мы запускаем функцию Attack каждую секунду у нашего противника
        {
            if (!IsInvoking("Attack"))
            {
                Invoke("Attack", 1);
            }
        }

        if (player == null) // избегаем ошибок, заменяя нашего игрока бесполезной камерой :)
        {
            player = GameObject.Find("Main Camera");
        }

        posToPlayer = (gameObject.transform.position.x + gameObject.transform.position.y) - (player.transform.position.x + player.transform.position.y);// переменная
		//содержащая расстояние от противника до игрока

        if (flag)// если флаг установлен запускаем функцию MoveToPlayer
        {
            MoveToPlayer();
        }
        else// если же не установлен флаг то выполнять Moving
            Moving();
    }

    void Flip()// функция, которая разворачивает объект на 180 градусов
    {
        gameObject.GetComponent<Transform>().Rotate(Vector2.up * 180);
    }

    void Moving()// функция передвижения противника
    {
        gameObject.transform.Translate(direction);
    }

    public void Attack()  // функция для атаки
    {
        anim.SetTrigger("Attack");// включаем анимацию с именем Attack
        player = GameObject.Find("Player");// ищем игрока на сцене и заносим его в переменную player

        if (posToPlayer <= 1.3f)//если расстояние до игрока меньше заданного, отнять единицу здоровья
        {
            MainSystem.playerHp--;
            if (MainSystem.playerHp == 0)// если хп равно нулю, уничтожить объект игрока
            {
                Destroy(player);
            }
        }
    }

    void MoveToPlayer()// функция для передвижения противника к игроку
    {
        transform.position = Vector2.Lerp(transform.position, player.transform.position, 0.009f);//присваиваем позиции противника интерполируемый 
		//вектор2 со значениями позиции противника до значений позиции игрока со скоростью заданной третьим параметром
    }
}