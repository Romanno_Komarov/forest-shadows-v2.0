﻿ using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainSystem : MonoBehaviour
{
    public Rigidbody2D rb;  // создаем переменную rb класса Rigidbody2D, которая будет в себе хранить 
    // текущий объект со сцены, который мы перенесем drag&drop-ом со сцены в ячейку rb

    public GameObject enemy; // создаем публичную переменную Enemy, куда код далее поместит Enemy1

    private float posToEnemy; // переменная для позиции между игроком и ближайшим енеми
    private int countEnemy = 1; // переменная для того чтобы заносилась правильно следующая цель для игрока

    public Animator anim; // переменная для обращения к Аниматору в объекте игрока

    static public int playerHp = 2; // создаем переменную для хп плеера 

    public float speedMove = 2.3f;  // создаем переменную, которая будет отвечать за скорость передвижения
    public float heightJump = 6.5f;  // создаем переменную, которая будет отвечать за высоту прыжка

    private bool isGround;  // создаем переменную типа bool, которая будет отвечать за отслеживание соприкосновения с землей, по дефолту она false

    private bool MoveRightButton; // логические переменные, которые отвечают за отслеживание касания кнопки
    private bool MoveLeftButton;

    public AudioClip attackSound; // переменная, содержащая в себе звук атаки

    void Start() 
    {
        anim = GetComponent<Animator>(); // ищем компонет Animator в игроке, если нашли заносим в anim
    }

    void OnCollisionEnter2D(Collision2D Obj) // создаем функцию, которая будет отвечать за соприкосновение объекта, 
    // на котором весит скрипт(Player) с объектом, Тег которого является "Ground";
    // это позволит нам задать лимит на кол-во прыжков
    {
        if (Obj.gameObject.tag == "Ground")  // если наш Player соприкоснулся с объектом, тег которого Ground, мы делаем нашу переменную isGround равной true
        {
            isGround = true;
        }
    }

    void MoveRight()  // функция для перемещения вправо 
    {
        rb.velocity = new Vector2(speedMove, rb.velocity.y);  // обращаемся к нашей переменной rb и создаем новый вектор, который будет получать на вход x(направление и скорость) и y(текущее положение объекта)
        gameObject.GetComponent<SpriteRenderer>().flipX = false;
    }

    void MoveLeft()  // функция для перемещения влево 
    {
        rb.velocity = new Vector2(-speedMove, rb.velocity.y);  // отличие только в знаке перед x, что позволяет нам задать направление вектора
        gameObject.GetComponent<SpriteRenderer>().flipX = true;  // находим компонент SpriteRenderer на нашем Player-е, и флипаем(поворот на 180 градусов по X) его
    }

    public void Jump() // делаем функцию публичной, так как нам нужно ее вызывать из сцены, по нажатию на кнопку
    {
        if (isGround)  // проверяем перед прыжком на земле ли мы, если на земле - выполняем прыжок и ставим isGround в false и мы не сможем прыгнуть пока isGround не будет true
        {
            rb.velocity = new Vector2(rb.velocity.x, heightJump);  // задаем высоту прыжка как y, x остается без изменений при нажатии на пробел 
            isGround = false;
        }
    }

    public void Attack()  // функция для атаки
    {
        anim.SetTrigger("Attack"); // в окне аниматор имеется связь анимации Idle с анимацией Attack, переход на Attack осуществляется включением триггера с именем Attack
		// что мы и делаем: включаем при атаке триггер Attack, в следсвии чего видим анимацию атаки у игрока
        if (posToEnemy >= -1.2f)// проверяем, если расстояние между игроком и енеми меньше заданного, тогда отнимаем у енеми хп
        {
            EnemySystem.enemyHp--;
            if (EnemySystem.enemyHp == 0)// если хп стало равно нулю, уничтожаем текущий объект и прибавляем к переменной 
			//countEnemy единицу, чтобы следующий енеми стал нашим следующий врагом
            {
                Destroy(enemy);
                countEnemy++;
            }
            Debug.Log("get damage" + EnemySystem.enemyHp); // check check check
        }
        AudioSource.PlayClipAtPoint(attackSound, transform.position); // проигрываем звук по нажатию на кнопку
    }

    void Update() // метод Update вызывается каждый кадр
    {

        enemy = GameObject.Find("Enemy" + countEnemy);// создаем переменную enemy в которую занесем объект с именем Enemy + перемнная, отвечающая за номер врага.

        if (enemy == null) // если никаких врагов больше на карте нет, присвоим enemy нашу Main Camera, чтобы избежать ошибок
        {
            enemy = GameObject.Find("Main Camera");
        }

        posToEnemy = (gameObject.transform.position.x + gameObject.transform.position.y) - (enemy.transform.position.x + enemy.transform.position.y);// переменная
		// для позиции между игроком и ближайшим енеми((координата x+y игрока) - (координата x+y enemy) = posToEnemy)

        if (gameObject.GetComponent<SpriteRenderer>().flipX)// здесь мы устанавлием значения для наших BoxCollider2D and CircleCollider2D, 
		//так как при флипе(повороте игрока на 180 градусов) наши коллайдеры смещаются, что не позволяет точно отследить столкновения
		//в следующих четырех строчках мы просто задаем координаты для x, y в двух коллайдерах при активном флипе и наоборот
        {
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.63f, 0.8372259f);
            gameObject.GetComponent<CircleCollider2D>().offset = new Vector2(-0.51f, -1.56f);
        }
        else
        {
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(0.655365f, 0.8372259f);
            gameObject.GetComponent<CircleCollider2D>().offset = new Vector2(0.77f, -1.56f);
        }

        if (Input.GetKey(KeyCode.D) || MoveRightButton)  // отслеживаем нажатие клавиши D
        {
            MoveRight();  // вызываем функцию MoveRight
        }

        if (Input.GetKey(KeyCode.A) || MoveLeftButton)  // отслеживаем нажатие клавиши A
        {
            MoveLeft();  // вызываем функцию MoveLeft
        }

        if (Input.GetKeyDown(KeyCode.Space))  // отслеживаем нажатие клавиши Space
        {
            Jump();  // вызываем функцию Jump
        }
    }

    public void RightDown()             // четыре функции, отвечающие за отслеживание касания по кнопке
    {                                   // четыре функции, отвечающие за отслеживание касания по кнопке
        MoveRightButton = true;         // четыре функции, отвечающие за отслеживание касания по кнопке
    }                                   // четыре функции, отвечающие за отслеживание касания по кнопке
    // четыре функции, отвечающие за отслеживание касания по кнопке
    public void RightUp()               // четыре функции, отвечающие за отслеживание касания по кнопке
    {                                   // четыре функции, отвечающие за отслеживание касания по кнопке
        MoveRightButton = false;        // четыре функции, отвечающие за отслеживание касания по кнопке
    }                                   // четыре функции, отвечающие за отслеживание касания по кнопке
    // четыре функции, отвечающие за отслеживание касания по кнопке
    public void LeftDown()              // четыре функции, отвечающие за отслеживание касания по кнопке
    {                                   // четыре функции, отвечающие за отслеживание касания по кнопке
        MoveLeftButton = true;          // четыре функции, отвечающие за отслеживание касания по кнопке
    }                                   // четыре функции, отвечающие за отслеживание касания по кнопке
    // четыре функции, отвечающие за отслеживание касания по кнопке
    public void LeftUp()                // четыре функции, отвечающие за отслеживание касания по кнопке
    {                                   // четыре функции, отвечающие за отслеживание касания по кнопке
        MoveLeftButton = false;         // четыре функции, отвечающие за отслеживание касания по кнопке
    }
}

